﻿"use strict";

const gulp = require("gulp"),
    rimraf = require("rimraf"),
    concat = require("gulp-concat"),
    cssmin = require("gulp-cssmin"),
    fs = require("fs"),
    less = require("gulp-less"),
    sass = require("gulp-sass");
    //watch = require('gulp-watch');

const paths = {
    webroot: "./wwwroot/"
};

paths.scss = paths.webroot + "css/**/*.scss";
paths.css = paths.webroot + "css/**/*.css";
paths.minCss = paths.webroot + "css/**/*.min.css";
paths.concatCssDest = paths.webroot + "css/site.min.css";

gulp.task("clean:css", done => rimraf(paths.concatCssDest, done));

gulp.task("scss", function () {
    return gulp.src(paths.scss)
        .pipe(sass())
        .pipe(gulp.dest('wwwroot/css'));
});

gulp.task("min:css", () => {
    return gulp.src([paths.css, "!" + paths.minCss])
        .pipe(concat(paths.concatCssDest))
        .pipe(cssmin())
        .pipe(gulp.dest("."));
});


// A 'default' task is required by Gulp v4
gulp.task("default", gulp.series(["scss", "min:css"]));

//gulp.task('watch', function () {
//    return watch('css/**/*.scss', { ignoreInitial: false })
//        .pipe(sass())
//        .pipe(gulp.dest('wwwroot/css'));
//        //.pipe(gulp.dest('default'));
//});

gulp.task('watch', function () {
    gulp.watch(paths.scss, gulp.series('scss'));
});