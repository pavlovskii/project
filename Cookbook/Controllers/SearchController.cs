﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cookbook.Data;
using Cookbook.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;


namespace Cookbook.Controllers
{
    public class SearchController : Controller
    {
        private readonly ApplicationDbContext _context;

        public SearchController(ApplicationDbContext context)
        {
            _context = context;
        }

        public ActionResult Index(string id)
        {
            var recipesId = LuceneSearch.Search(id, null).Select(i=>i.RecipeId).ToList();
            List<Recipe> findRecipes = new List<Recipe>();
            foreach (var recipeId in recipesId)
            {
                var recipe = _context.Recipes.Include(a => a.Author).FirstOrDefault(i => i.RecipeId == recipeId);
                findRecipes.Add(recipe);
            }
            return View(findRecipes);
        }
    }
}
