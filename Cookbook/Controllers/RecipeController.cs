﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Cookbook.Data;
using Cookbook.Models;
using Imgur.API.Authentication.Impl;
using Imgur.API.Endpoints.Impl;
using Imgur.API.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Cookbook.Controllers
{
    [Authorize]
    public class RecipeController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IHostingEnvironment _hostingEnvironment;

        public RecipeController(ApplicationDbContext context, UserManager<ApplicationUser> userManager, IHostingEnvironment hostingEnvironment)
        {
            _context = context;
             _userManager = userManager;
            _hostingEnvironment = hostingEnvironment;
        }

        public async Task<IActionResult> Edit(int id)
        {

            var recipe = await _context.Recipes.Include(a => a.IngredientsForRecipe).FirstOrDefaultAsync(m => m.RecipeId == id);
            var user = await _userManager.GetUserAsync(User);
            if (recipe != null && (user == recipe.Author || User.IsInRole("Admin")))
            {
                return View(recipe);
            }

            return NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> Edit(int id, Recipe recipeModel)
        {
            var user = await _userManager.GetUserAsync(User);
            var recipe = await _context.Recipes.Include(i=>i.IngredientsForRecipe).FirstOrDefaultAsync(m => m.RecipeId == id);
            if (user == recipe.Author || User.IsInRole("Admin"))
            {
                TagCleaner(recipe.IngredientsForRecipe);
                TagHelper(recipeModel.IngredientsForRecipe);
                recipe.RecipeName = recipeModel.RecipeName;
                recipe.Category = recipeModel.Category;
                recipe.Abstract = recipeModel.Abstract;
                recipe.Description = recipeModel.Description;
                recipe.IngredientsForRecipe = recipeModel.IngredientsForRecipe;
                recipe.LastEditTime = DateTime.Now;
                await _context.SaveChangesAsync();
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<IActionResult> RateRecipe(int id, int rating)
        {
            var recipe = await _context.Recipes.Include(a => a.Author).Include(r => r.RatesUsers)
                .FirstOrDefaultAsync(i => i.RecipeId == id);
            var user = await _userManager.GetUserAsync(User);
            var ratedUsersId = recipe.RatesUsers.Select(i => i.Author).ToList();

            if (ratedUsersId.Contains(user))
            {
                var rates = recipe.RatesUsers.Where(i => i.Author == user).ToList();
                foreach (var rate in rates)
                {
                    recipe.RatesUsers.Remove(rate);                    
                }
            }

            recipe.RatesUsers.Add(new RateUser()
            {
                RecipeId = id,
                Author = user,
                Rate = rating
            });

            recipe.Rating = (double) recipe.RatesUsers.Sum(x => x.Rate) / recipe.RatesUsers.Count;
            
            await _context.SaveChangesAsync();

            return RedirectToAction("Details/" + id);
        }

        [HttpPost]
        public async Task<IActionResult> CommentLike(int id, int recipeId)
        {
            var comment = await _context.Comments.Include(a => a.Author).Include(l => l.CommentLikes)
                .FirstOrDefaultAsync(i => i.CommentId == id);
            var user = await _userManager.GetUserAsync(User);

            var likedUsersId = comment.CommentLikes.Select(i => i.Author).ToList();

            if (likedUsersId.Contains(user))
            {
                comment.Likes -= 1;
                var like = await _context.CommentLikes.FirstOrDefaultAsync(i => i.Author == user);
                comment.CommentLikes.Remove(like);
            }
            else
            {
                comment.Likes += 1;
                comment.CommentLikes.Add(new CommentLike()
                {
                    Author = user,
                    CommentId = id
                });
            }

            await _context.SaveChangesAsync();

            return RedirectToAction("Details/" + recipeId);
        }

        [HttpPost]
        public async Task<IActionResult> AddComment(int postId, Recipe recipe)
        {
            var commentedRecipe = await _context.Recipes.FirstOrDefaultAsync(m => m.RecipeId == postId);
            if (commentedRecipe == null)
            {
                return NotFound();
            }

            commentedRecipe.CurrentComment = recipe.CurrentComment;

            _context.Comments.Add(new Comment()
            {
                RecipeId = postId,
                Author = await _userManager.GetUserAsync(User),
                CreatedDate = DateTime.UtcNow,
                Text = recipe.CurrentComment
            });
            await _context.SaveChangesAsync();
            LuceneSearch.AddUpdateLuceneIndexComment(commentedRecipe);
            return Redirect("Details/" + postId);
        }

        [AllowAnonymous]
        public async Task<IActionResult> Index(string sortBy)
        {
            var viewModel = new RecipeIndexViewModel();
            if (sortBy == null)
            {
                viewModel.Recipes = await _context.Recipes.Include(a => a.Author).OrderByDescending(t=>t.LastEditTime).ToListAsync();
            }
            if (sortBy == "rating")
            {
                viewModel.Recipes = await _context.Recipes.Include(a => a.Author).OrderByDescending(r=>r.Rating).ToListAsync();
            }
            if (sortBy == "name")
            {
                viewModel.Recipes = await _context.Recipes.Include(a => a.Author).OrderBy(n => n.RecipeName).ToListAsync();
            }
            if (sortBy == "date")
            {
                viewModel.Recipes = await _context.Recipes.Include(a => a.Author).OrderBy(t=>t.LastEditTime).ToListAsync();
            }

            viewModel.Tags = await _context.Tags.ToListAsync();
            return View(viewModel);
        }

        [AllowAnonymous]
        public async Task<IActionResult> Categories()
        {
            var categories = await _context.Recipes.Select(c => c.Category).Distinct().OrderBy(x=>x).ToListAsync();
            return View(categories);
        }

        [AllowAnonymous]
        public async Task<IActionResult> SortByCategory(string id)
        {
            var viewModel = new CategoryViewModel
            {
                CategoryName = id,
                Recipes = await _context.Recipes.Include(a => a.Author).Where(c => c.Category == id).ToListAsync()
            };


            return View(viewModel);
        }

        [AllowAnonymous]
        public async Task<IActionResult> SortByTag(int id)
        {
            var viewModel = new RecipeIndexViewModel();

            var tag = await _context.Tags.FirstOrDefaultAsync(i => i.TagId == id);
            var recipesId = await _context.IngredientForRecipes.Where(n => n.IngredientName == tag.TagName)
                .Select(i => i.RecipeId).Distinct().ToListAsync();

            viewModel.Recipes = await _context.Recipes.Include(a => a.Author).Where(i => recipesId.Contains(i.RecipeId)).ToListAsync();

            viewModel.Tags = new List<Tag>() {tag};

            return View(viewModel);
        }

        [AllowAnonymous]
        public async Task<IActionResult> Details(int id)
        {
            var post = await _context.Recipes.Include(a => a.Author).Include(i =>i.IngredientsForRecipe).FirstOrDefaultAsync(m => m.RecipeId == id);

            if (post == null)
            {
                return NotFound();
            }

            var comments = _context.Comments.Include(a=>a.Author).Where(x => x.RecipeId == id).ToList();
            post.Comments = comments;

            return View(post);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Recipe recipe)
        {
            if (ModelState.IsValid)
            {
                TagHelper(recipe.IngredientsForRecipe);
                recipe.LastEditTime = DateTime.Now;
                recipe.Author = await _userManager.GetUserAsync(User);
                _context.Add(recipe);
                await _context.SaveChangesAsync();
                LuceneSearch.AddUpdateLuceneIndex(recipe);
                return RedirectToAction("Index");
            }
            return View(recipe);
        }

        public async Task<IActionResult> Delete(int id)
        {

            var recipe = await _context.Recipes.FirstOrDefaultAsync(m => m.RecipeId == id);
            var user = await _userManager.GetUserAsync(User);
            if (recipe != null && (user == recipe.Author || User.IsInRole("Admin")))
            {
                return View(recipe);
            }

            return NotFound();
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id, ApplicationUser user)
        {
            var recipe = await _context.Recipes.Include(a => a.RatesUsers).Include(a => a.Comments)
                            .Include(a => a.Author).Include(i=>i.IngredientsForRecipe).FirstOrDefaultAsync(m => m.RecipeId == id);

            if (user == null)
            {
                user = await _userManager.GetUserAsync(User);
            }

            if (user == recipe.Author || User.IsInRole("Admin"))
            {
                TagCleaner(recipe.IngredientsForRecipe);
                foreach (var comment in recipe.Comments)
                {
                    _context.Remove(comment);
                }
                foreach (var ingredient in recipe.IngredientsForRecipe)
                {
                    _context.Remove(ingredient);
                }

                _context.Recipes.Remove(recipe);
                await _context.SaveChangesAsync();
            }

            return RedirectToAction("Index");
        }

        public ActionResult AutocompleteIngredients(string term)
        {
            var ingredientList = _context.IngredientForRecipes.Where(i => i.IngredientName.Contains(term))
                .Select(x => x.IngredientName).Distinct().ToArray();
            return Json(ingredientList);
        }

        public ActionResult AutocompleteCatergory(string term)
        {
            var categoryList = _context.Recipes.Where(i => i.Category.Contains(term))
                .Select(x => x.Category).Distinct().ToArray();
            return Json(categoryList);
        }

        [HttpPost]
        public async Task<string> UploadImage(IFormFile file)
        {
            var client = new ImgurClient(
                "6f723ed132879e0",
                "d2226573ab16224ab0901382ebc4e75fbca58357"
                );
            var endpoint = new ImageEndpoint(client);
            IImage image;
            using (var fileStream = file.OpenReadStream())
            {
                using (var ms = new MemoryStream())
                {
                    fileStream.CopyTo(ms);
                    var fileBytes = ms.ToArray();
                    string s = Convert.ToBase64String(fileBytes);
                    image = await endpoint.UploadImageBinaryAsync(fileBytes);
                }
            }

            return image.Link;
        }

        private void TagHelper(List<IngredientForRecipe> repeatingIngredients)
        {
            var ingredients = repeatingIngredients.Select(n => n.IngredientName).Distinct().ToList();
            foreach (var ingredient in ingredients)
            {
                var tag = _context.Tags.FirstOrDefault(x => x.TagName == ingredient);
                if (tag != null)
                {
                    tag.Count++;
                    tag.HTMLStyle = TagSizeChanger(tag.Count);
                }
                else
                {
                    var newTag = new Tag()
                    {
                        TagName = ingredient,
                        Count = 1,
                        HTMLStyle = "x-small"
                    };
                    _context.Tags.Add(newTag);
                }
            }
            _context.SaveChanges();
        }

        private void TagCleaner(List<IngredientForRecipe> repeatingIngredients)
        {
            var ingredients = repeatingIngredients.Select(n => n.IngredientName).Distinct().ToList();
            foreach (var ingredient in ingredients)
            {
                var tag = _context.Tags.FirstOrDefault(x => x.TagName == ingredient);
                if (tag != null)
                {
                    if (tag.Count > 1)
                    {
                        tag.Count--;
                        tag.HTMLStyle = TagSizeChanger(tag.Count);
                    }
                    else
                    {
                        _context.Tags.Remove(tag);
                    }
                }
            }
            _context.SaveChanges();
        }

        private string TagSizeChanger(int count)
        {
            switch (count)
            {
                case 1:
                    return "x-small";
                case 2:
                    return "small";
                case 3:
                    return "medium";
                case 4:
                    return "large";
                case 5:
                    return "x-large";
                default:
                    return "xx-large";
            }
        }
    }
}