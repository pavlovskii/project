﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cookbook.Models
{
    public class CategoryViewModel
    {
        public string CategoryName { get; set; }
        public List<Recipe> Recipes { get; set; }
    }
}
