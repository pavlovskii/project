﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cookbook.Models
{
    public class CommentLike
    {
        public int CommentLikeId { get; set; }
        public ApplicationUser Author { get; set; }
        public int CommentId { get; set; }
    }
}
