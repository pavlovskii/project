﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cookbook.Models
{
    public class RecipeIndexViewModel
    {
        public List<Recipe> Recipes { get; set; }
        public List<Tag> Tags { get; set; }
    }
}
