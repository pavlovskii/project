﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cookbook.Models
{
    public class RateUser
    {
        public int RateUserId { get; set; }
        public int RecipeId { get; set; }
        public ApplicationUser Author { get; set; }
        public int Rate { get; set; }
    }
}
