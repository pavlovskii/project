﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Cookbook.Data.Migrations
{
    public partial class Tags : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Comments_Recipes_RecipeId1",
                table: "Comments");

            migrationBuilder.DropForeignKey(
                name: "FK_IngredientForRecipe_Recipes_RecipeId",
                table: "IngredientForRecipe");

            migrationBuilder.DropPrimaryKey(
                name: "PK_IngredientForRecipe",
                table: "IngredientForRecipe");

            migrationBuilder.DropIndex(
                name: "IX_Comments_RecipeId1",
                table: "Comments");

            migrationBuilder.DropColumn(
                name: "RecipeId1",
                table: "Comments");

            migrationBuilder.RenameTable(
                name: "IngredientForRecipe",
                newName: "IngredientForRecipes");

            migrationBuilder.RenameIndex(
                name: "IX_IngredientForRecipe_RecipeId",
                table: "IngredientForRecipes",
                newName: "IX_IngredientForRecipes_RecipeId");

            migrationBuilder.AlterColumn<int>(
                name: "RecipeId",
                table: "Comments",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CommentId",
                table: "Comments",
                nullable: false,
                oldClrType: typeof(string))
                .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_IngredientForRecipes",
                table: "IngredientForRecipes",
                column: "IngredientForRecipeId");

            migrationBuilder.CreateTable(
                name: "Tags",
                columns: table => new
                {
                    TagId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Count = table.Column<int>(nullable: false),
                    HTMLStyle = table.Column<string>(nullable: true),
                    TagName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tags", x => x.TagId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Comments_RecipeId",
                table: "Comments",
                column: "RecipeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Comments_Recipes_RecipeId",
                table: "Comments",
                column: "RecipeId",
                principalTable: "Recipes",
                principalColumn: "RecipeId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_IngredientForRecipes_Recipes_RecipeId",
                table: "IngredientForRecipes",
                column: "RecipeId",
                principalTable: "Recipes",
                principalColumn: "RecipeId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Comments_Recipes_RecipeId",
                table: "Comments");

            migrationBuilder.DropForeignKey(
                name: "FK_IngredientForRecipes_Recipes_RecipeId",
                table: "IngredientForRecipes");

            migrationBuilder.DropTable(
                name: "Tags");

            migrationBuilder.DropPrimaryKey(
                name: "PK_IngredientForRecipes",
                table: "IngredientForRecipes");

            migrationBuilder.DropIndex(
                name: "IX_Comments_RecipeId",
                table: "Comments");

            migrationBuilder.RenameTable(
                name: "IngredientForRecipes",
                newName: "IngredientForRecipe");

            migrationBuilder.RenameIndex(
                name: "IX_IngredientForRecipes_RecipeId",
                table: "IngredientForRecipe",
                newName: "IX_IngredientForRecipe_RecipeId");

            migrationBuilder.AlterColumn<string>(
                name: "RecipeId",
                table: "Comments",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<string>(
                name: "CommentId",
                table: "Comments",
                nullable: false,
                oldClrType: typeof(int))
                .OldAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            migrationBuilder.AddColumn<int>(
                name: "RecipeId1",
                table: "Comments",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_IngredientForRecipe",
                table: "IngredientForRecipe",
                column: "IngredientForRecipeId");

            migrationBuilder.CreateIndex(
                name: "IX_Comments_RecipeId1",
                table: "Comments",
                column: "RecipeId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Comments_Recipes_RecipeId1",
                table: "Comments",
                column: "RecipeId1",
                principalTable: "Recipes",
                principalColumn: "RecipeId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_IngredientForRecipe_Recipes_RecipeId",
                table: "IngredientForRecipe",
                column: "RecipeId",
                principalTable: "Recipes",
                principalColumn: "RecipeId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
